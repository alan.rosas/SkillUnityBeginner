# SkillUnityBeginner
Puede utilizar sin asistencia el editor, maneja los ciclos de vida, construye los objetos que componen Unity :
- Utiliza GameObjects, Transforms, Prefabs, Scenes, MonoBehaviours y sus ciclos de vida (Awake, Start, Update, LateUpdate).
- Sabe comunicar componentes de Unity entre sí
- Sabe cómo cargar e instanciar assets en Runtime.
- Sabe hacer y reproducir animaciones (uso básico de Animator y Animation)
- Sabe crear y editar prefabs y nested prefabs
- Sabe utilizar persistencia local (playerPrefs)
- Conoce el valor que aportan los archivos .meta
- Mantiene un orden con los archivos/carpetas, así como con los nombres
- Conoce las diferentes configuraciones de cámara (perspectiva, ortográfica, FOV, clear flags)
- Sabe utilizar el sistema de input (teclado, mouse, touch, event system)
- Sabe como reproducir sonidos (Audio Listener, Audio Source)
- Entiende y Utiliza los distintos tipos de canvas 
- Puede generar UIs responsive
- Sabe cómo hacer editor tests dentro de Unity
- Puede trabajar con un proyecto Unity en un repositorio: Que archivos generan conflictos, cuáles subir y cuáles ignorar
- Utiliza algún sistema de paquetizado para obtener nuevos features/sistemas (upm etermax, upm unity, asset store)

